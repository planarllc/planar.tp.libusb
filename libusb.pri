message("libusb-1.0")
message("libusb-1.0 library location:" $$PWD)

INCLUDEPATH += $$PWD/include/libusb-1.0
DEPENDPATH += $$PWD

win32{
        win32-msvc{
            contains(QMAKE_TARGET.arch, "x86"){                     #x86
            LIBS += -L$$PWD/bin/MS32/static/ -llibusb-1.0
            DEPENDPATH += $$PWD/bin/MS32/static/
            message("libusb-1.0 windows-msvc-x86")
            } else {                                                #x64
                LIBS += -L$$PWD/bin/MS64/static/ -llibusb-1.0
                DEPENDPATH += $$PWD/bin/MS64/static/
                message("libusb-1.0 windows-msvc-x64")
                }
        } else {
            win32-g++{                                              # mingw x86 only
            LIBS += -L$$PWD/bin/MinGW32/static/ -llibusb-1.0
            DEPENDPATH += $$PWD/bin/MinGW32/static/
            message("libusb-1.0 windows-mingw-x86")
            }
        }
} else {
    unix {
        LIBS += -lusb-1.0
        message("libusb-1.0 linux")
        }
}

